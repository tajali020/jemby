import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import org.zap.gemby 1.0

Page {
    Authentication {
        id: authentication
    }

    title: qsTr("Add Server")

    ColumnLayout {
        spacing: units.gu(2)
        anchors {
            margins: units.gu(2)
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Item {
            Layout.fillHeight: true
        }

        TextField {
            id: serverField
            width: parent.width*0.8
            height: units.gu(5)
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("Server")
        }

        TextField {
            id: usernameField
            width: parent.width*0.8
            height: units.gu(5)
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("Username")
        }

        TextField {
            id: passwordField
            width: parent.width*0.8
            height: units.gu(5)
            anchors.horizontalCenter: parent.horizontalCenter
            echoMode: TextInput.Password
            placeholderText: qsTr("Password")
        }

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: i18n.tr('Add Server!')
            onClicked: authentication.speak(serverField.text, usernameField.text, passwordField.text)
        }

        Item {
            Layout.fillHeight: true
        }
    }
}
