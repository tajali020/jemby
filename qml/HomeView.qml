import QtQuick 2.7
import QtQuick.Controls 2.2

Page {
    anchors {
        margins: units.gu(2)
        top: header.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
    }

    title: qsTr("Home")

    Label {
        text: qsTr("You are on the home page.")
        anchors.centerIn: parent
    }
}
