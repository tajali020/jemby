import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import org.zap.gemby 1.0

Page {
    title: "Add Server"

    Song {
        id: song
    }

    ColumnLayout {
        Button {
            Layout.alignment: Qt.AlignHCenter
            text: 'Add Server!'
            onClicked: song.play()
        }
    }
}
