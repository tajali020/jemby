#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQmlProperty>
#include <QCryptographicHash>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include <QMediaPlaylist>
#include <QMediaPlayer>

#include <QFileInfo>
#include <QStandardPaths>
#include <QDir>

#include "authentication.h"
#include "configstore.h"
#include "databasemanager.h"

QString embyusr = "";
QString embykey = "";
QString CLIENT = "Gemby";
QString device_id = "xxx";

Authentication::Authentication() {

}

void Authentication::speak(QString server, QString username, QString password) {
    setServer(server); setUsername(username); setPassword(password);
    qDebug() << getServer() << ": " << getUsername();
    QString endpoint = "Users/authenticatebyname";
    QUrl url = QUrl(getServer()+"/"+endpoint);
    QNetworkRequest req(url);

    QByteArray postData;
    postData.append("Username="+getUsername()+"&");
    postData.append("Pw="+getPassword());

    qDebug() << url;
    qDebug() << postData;

    req.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    req.setRawHeader("X-Emby-Authorization", "MediaBrowser Client=\"" + CLIENT.toUtf8() +"\", Device=\"Python Script\", DeviceId=\"" + generateDeviceId().toUtf8() +"\", Version=\"1.0.0.0\"");
    QNetworkAccessManager *man = new QNetworkAccessManager(this);
    connect(man, &QNetworkAccessManager::finished, this, &Authentication::callback);
    man->post(req, postData);
}

void Authentication::callback(QNetworkReply* reply) {
    if(reply->error() == QNetworkReply::NoError) {
        QJsonParseError jsonError;
        QJsonDocument loadDoc = QJsonDocument::fromJson(reply->readAll(), &jsonError);
        if(!jsonError.error) {
            DatabaseManager db = DatabaseManager();
            QJsonObject jsonObj = loadDoc.object();
            db.addServer(jsonObj["ServerId"].toString(),
                    jsonObj["SessionInfo"].toObject()["UserId"].toString(),
                    jsonObj["AccessToken"].toString(),
                    jsonObj["SessionInfo"].toObject()["DeviceId"].toString());
            db.printAllServers();
        } else {
            qDebug() << jsonError.errorString();
        }
    }
    else {
        qDebug() << reply->errorString();
    }
}

QString Authentication::generateDeviceId() {
    QFileInfo fi(QDir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation)).absolutePath());
    QByteArray volatile_seed = QString::number(fi.created().toMSecsSinceEpoch()).toUtf8();

    QByteArray data_1 = QCryptographicHash::hash(
                        QString(getUsername()+getPassword()).toUtf8(),
                        QCryptographicHash::Md5).toHex();

    QString data_2 = QString(QCryptographicHash::hash(
                QString(data_1+volatile_seed).toUtf8(),
                QCryptographicHash::Md5).toHex());

    return data_2.left(16);
}
