#ifndef AUTHENTICATION_H
#define AUTHENTICATION_H

#include <QObject>
#include <QNetworkReply>

class Authentication: public QObject {
    Q_OBJECT

public:
    Authentication();
    ~Authentication() = default;

    Q_INVOKABLE void speak(QString server, QString username, QString password);

    QString getServer() {return server;};
    void setServer(QString server) {this->server = server;};

    QString getUsername() {return username;};
    void setUsername(QString username) {this->username = username;};

    QString getPassword() {return password;};
    void setPassword(QString password) {this->password=password;};

private slots:
    void callback(QNetworkReply*);

private:
    QString generateDeviceId();
    QString server;
    QString username;
    QString password;
};

#endif // AUTHENTICATION_H
