#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>

class DatabaseManager
{
public:
    DatabaseManager();
    bool addServer(const QString& server, const QString& userId, const QString& accessToken, const QString& deviceId);
    void printAllServers();

private:
    QSqlDatabase database;
    bool createTables();
};

#endif // DATABASEMANAGER_H
