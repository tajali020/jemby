#include <QStandardPaths>
#include <QDir>
#include <QDebug>

#include "databasemanager.h"

DatabaseManager::DatabaseManager()
{
    QString path = QDir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation)).absolutePath() + "/db.sqlite";
    database = QSqlDatabase::addDatabase("QSQLITE");
    database.setDatabaseName(path);

    if (!database.open())
    {
        qDebug() << "Error: connection with database failed";
    }
    else
    {
        qDebug() << "Database: connection ok, opened " << path;
    }
    createTables();
}

bool DatabaseManager::createTables()
{
    bool success = false;
    // you should check if args are ok first...
    if (database.tables(QSql::AllTables).contains( QLatin1String("server")))
        success = true;
    else {
        QSqlQuery query;
        query.prepare("CREATE TABLE server (id INTEGER PRIMARY KEY, serverId TEXT NOT NULL, userId TEXT NOT NULL, accessToken TEXT NOT NULL, deviceId TEXT NOT NULL);");
        if(query.exec())
        {
            success = true;
        }
        else
        {
             qDebug() << "createTables error:" << query.lastError();
        }
    }
    return success;
}

bool DatabaseManager::addServer(const QString& serverId, const QString& userId, const QString& accessToken, const QString& deviceId)
{
   bool success = false;
   // you should check if args are ok first...
   QSqlQuery query;
   query.prepare("INSERT INTO server (serverId, userId, accessToken, deviceId) VALUES (:serverId, :userId, :accessToken, :deviceId);");
   query.bindValue(":serverId", serverId);
   query.bindValue(":userId", userId);
   query.bindValue(":accessToken", accessToken);
   query.bindValue(":deviceId", deviceId);
   if(query.exec())
   {
       success = true;
   }
   else
   {
        qDebug() << "addServer error:" << query.lastError();
   }

   return success;
}

void DatabaseManager::printAllServers()
{
    QSqlQuery query("SELECT * FROM server");
    int serverId = query.record().indexOf("serverId");
    int userId = query.record().indexOf("userId");
    int AccessToken = query.record().indexOf("AccessToken");
    int deviceId = query.record().indexOf("deviceId");
    while (query.next())
    {
        QString server = query.value(serverId).toString();
        QString user = query.value(userId).toString();
        QString token = query.value(AccessToken).toString();
        QString device = query.value(deviceId).toString();
        qDebug() << server << " " << user << " " << token << " " << device;
    }
}
