import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'jemby.zap'
    automaticOrientation: true
    backgroundColor: styleMusic.mainView.backgroundColor
    theme.name: "Ubuntu.Components.Themes.SuruDark"

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent
        title: qsTr("Stack")

        header: ToolBar {
            contentHeight: toolButton.implicitHeight

            ToolButton {
                id: toolButton
                text: stackView.depth > 1 ? "\u25C0" : "\u2630"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    if (stackView.depth > 1) {
                        stackView.pop()
                    } else {
                        drawer.open()
                    }
                }
            }

            Label {
                text: stackView.currentItem.title
                anchors.centerIn: parent
            }
        }

        Drawer {
            id: drawer
            width: window.width * 0.66
            height: window.height

            Column {
                anchors.fill: parent

                ItemDelegate {
                    text: qsTr("Add Server")
                    width: parent.width
                    onClicked: {
                        stackView.push("LoginView.qml")
                        drawer.close()
                    }
                }

                ItemDelegate {
                    text: qsTr("Songs")
                    width: parent.width
                    onClicked: {
                        stackView.push("NowPlayingView.qml")
                        drawer.close()
                    }
                }
            }
        }

        StackView {
            id: stackView
            initialItem: "HomeView.qml"
            anchors.fill: parent
            width: parent.width
        }
    }
}
